//
//  NewPaymentViewController.m
//  Owe
//
//  Created by Patrick Farrell on 12/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "NewPaymentViewController.h"
#import "NSString+Additions.h"
#import "OweDBDAO.h"

@interface NewPaymentViewController ()

@property (strong, nonatomic) NSString * currencyString;
@property (strong, nonatomic) IBOutlet UILabel *labelOweToUsername;
@property (strong, nonatomic) IBOutlet UILabel *labelOweDateCreated;
@property (strong, nonatomic) IBOutlet UILabel *labelPaymentDate;
@property (strong, nonatomic) IBOutlet UILabel *labelOweAmountTotal;
@property (strong, nonatomic) IBOutlet UILabel *labelAmountRemaining;
@property (strong, nonatomic) IBOutlet UILabel *labelCurrency;
@property (strong, nonatomic) IBOutlet UITextField *textFieldAmountPaying;
@property (strong, nonatomic) IBOutlet UITextField *textFieldOweDescription;

@end

@implementation NewPaymentViewController

#pragma mark - LifeCycle Methods

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    if (self)
    {
        [self setTitle:@"New Payment"];
        [self setCurrencyString:[NSString localCurrencySymbolString]];
        [self addLeftBarButtonCancel];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // configure the view
    [self configureViewForUse];
}


#pragma mark - UITextField Methods

// Called when editing did begin on a UITextField (AmountTotal field)
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //add save button to nav bar
    UIBarButtonItem * rightBarButtonItem = [[UIBarButtonItem alloc] init];
    [rightBarButtonItem setAction:@selector(saveOwePayment)];
    [rightBarButtonItem setEnabled:YES];
    [rightBarButtonItem setTarget:self];
    [rightBarButtonItem setTitle:@"Save"];
    
    // then we add the button to the navigation bar
    [[self navigationItem] setRightBarButtonItem:rightBarButtonItem];
}

// Called when text is being changed in the UITextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}


#pragma mark - Instance Methods

// Configure the View
- (void) configureViewForUse
{
    [[self labelCurrency] setText:[self currencyString]];
    
    // set up the view with any UIView customisations and then set any values
    [self prepareViewForUse];
    [self setUIValuesInView];
}

// Prepare the view and its subviews (set colors, fonts etc)
- (void) prepareViewForUse
{
    //adjusting font sizes to fit width
    [[self labelOweAmountTotal] setAdjustsFontSizeToFitWidth:YES];
    [[self labelAmountRemaining] setAdjustsFontSizeToFitWidth:YES];
    [[self labelOweToUsername] setAdjustsFontSizeToFitWidth:YES];
    [[self labelOweDateCreated] setAdjustsFontSizeToFitWidth:YES];
}

// Populate the subviews values if needed
- (void) setUIValuesInView
{
    //set the values representing the viewing owe
    [[self labelOweToUsername] setText:[[self viewingOwe] oweToUsername]];
    [[self labelCurrency] setText:[NSString localCurrencySymbolString]];
    [[self textFieldOweDescription] setText:[[self viewingOwe] description]];
    
    float amountTotal = [[self viewingOwe] amountTotal];
    NSString* amountTotalStr = [NSString stringWithFormat:@"%.02f", amountTotal];
    NSString * amountTotalWithCurrency = [[self currencyString] stringByAppendingString:amountTotalStr];
    [[self labelOweAmountTotal] setText:amountTotalWithCurrency];
  
    float amountRemaining = [[self viewingOwe] amountRemaining];
    NSString* amountRemainingStr = [NSString stringWithFormat:@"%.02f", amountRemaining];
    NSString * amountRemainingWithCurrency = [[self currencyString] stringByAppendingString:amountRemainingStr];
    NSString * amountRemainingLabelStr =
                        [@"Remaining: " stringByAppendingString:amountRemainingWithCurrency];
    [[self labelAmountRemaining] setText:amountRemainingLabelStr];
    
    
    NSString * dateCreated = [self getStringValueForShortDate:[[self viewingOwe] dateCreated]];
    [[self labelOweDateCreated] setText:dateCreated];

    NSString * paymentDate = [self getStringValueForShortDate:[NSDate date]];
    [[self labelPaymentDate] setText:paymentDate];
}

// Returns a String representation of a ShortStyle NSDate
- (NSString *) getStringValueForShortDate:(NSDate *) date
{
    return [NSDateFormatter localizedStringFromDate:date
                                                    dateStyle:NSDateFormatterShortStyle
                                                    timeStyle:NSDateFormatterNoStyle];
}

// Add the LeftBarButtonItem to the Navigation Bar
- (void) addLeftBarButtonCancel
{
    UIBarButtonItem * cancelButton = [[UIBarButtonItem alloc] init];
    [cancelButton setAction:@selector(closePaymentViewController)];
    [cancelButton setTitle:@"Close"];
    [cancelButton setTarget:self];
    
    // then we add the button to the navigation bar
    [[self navigationItem] setLeftBarButtonItem: cancelButton];
}

// Close this ViewController
- (void) closePaymentViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Selector pressed when user is done editing the Payment UITextField
- (void) okPressedPaymentTextField
{
    [[self textFieldAmountPaying] resignFirstResponder];
}

// Creates and returns an OwePayment Object from the relevant UI Values & the viewing Owe
- (OwePayment *) createOwePaymentToSave
{
    OwePayment * payment = [[OwePayment alloc] init];
    
    [payment setOwePaymentID:[NSString createGUIDString]];
    [payment setOweID:[[self viewingOwe] oweID]];
    [payment setAmountPaid:[[[self textFieldAmountPaying] text] floatValue]];
    [payment setDatePaid:[NSDate date]];
    
    return payment;
}

// Saves the Owe Payment for the parent Owe
- (void) saveOwePayment
{
    //create the payment from the UI Values
    OwePayment * payment = [self createOwePaymentToSave];
    
    //subtract the payment from the owe.amountremaining
    float newAmountRemaining = ( [[self viewingOwe] amountRemaining] - [payment amountPaid] );
    
    if(newAmountRemaining < 0)
    {
        // popup that paying too much.
        
        NSString* amountRemainingStr = [NSString stringWithFormat:@"%.02f", [[self viewingOwe] amountRemaining]];
        NSString * amountRemainingWithCurrency = [[self currencyString] stringByAppendingString:amountRemainingStr];
        
        
        NSString * alertTitle = @"Payment Invalid";
        NSString * alertMessage = [NSString stringWithFormat:@"You only have %@ remaining to pay.",amountRemainingWithCurrency];
        
        
        
        UIAlertView * message = [[UIAlertView alloc] initWithTitle:alertTitle
                                                           message:alertMessage
                                                           delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil, nil];
        
        [message show];
    }
    else
    {
        //make the payment
        
        //save the owe payment in the database
        OweDBDAO * dao = [OweDBDAO sharedInstance];
        [dao openDB];
        [dao createOwePaymentInDB:payment];
        [dao updateOwe:[payment oweID] withNewAmountRemaining:newAmountRemaining];
        [dao closeDB];
        
        //close this view controller and return to OwesList
        [self closePaymentViewController];
        
    }
}

@end
