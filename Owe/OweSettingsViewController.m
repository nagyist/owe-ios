//
//  OweSettingsViewController.m
//  Owe
//
//  Created by Patrick Farrell on 04/01/2014.
//  Copyright (c) 2014 PatrickFarrellApps. All rights reserved.
//

#import "OweSettingsViewController.h"
#import "OweSettingsPlistLoader.h"
#import "OweSetting.h"

@interface OweSettingsViewController ()

@property (strong, nonatomic) NSArray * settingsOptions;

@end

@implementation OweSettingsViewController
@synthesize tableView_settings;

#pragma mark - LifeCycle Methods

- (id)init
{
    self = [super initWithNibName:@"OweSettingsViewController" bundle:nil];
    if (self)
    {
        //custom initialisation here
        [self setTitle:@"Settings"];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self tableView_settings] setDelegate:self];
    [[self tableView_settings] setDataSource:self];

    //populate the data that the tableview will use
    [self populateTableData];
    
    //add the done button to the navigation bar
    [self addRightBarButtonItem];
    
}

#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = [[self settingsOptions] count];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //static the reuse id of the cell.
    static NSString * CellIdentifier = @"Cell";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    //get the setting at this index path
    OweSetting * setting = [[self settingsOptions] objectAtIndex:[indexPath row]];
    
    //set the text of the cell
    [[cell textLabel] setText:[setting settingTitle]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //get the setting at this index path
    //OweSetting * setting = [[self settingsOptions] objectAtIndex:[indexPath row]];
    
    //id oweConnectVC = [[NSClassFromString(@"OweConnectViewController") alloc] init];
}


# pragma mark - Instance Methods

// Adds a Button to the RHS of the Navigation Bar
- (void) addRightBarButtonItem
{
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]init];
    [doneButton setAction:@selector(rightBarButtonItemPressed)];
    [doneButton setTitle:@"Done"];
    [doneButton setTarget:self];
    
    // then we add the button to the navigation bar
    [[self navigationItem] setRightBarButtonItem: doneButton];
}

// Called when the RHS Bar Button Item is pressed
- (void) rightBarButtonItemPressed
{
    // TODO: Look into the completion block when this action is completed to
    // this might allow me to concurrently update some parse information
    // in the background after the settings modal view has been closed
    
    //dismiss this view controller
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Populates the array that the tableview uses
- (void) populateTableData
{
    //create instance of the settings plist loader class
    OweSettingsPlistLoader * settingsLoader = [[OweSettingsPlistLoader alloc] initWithPlist:@"OweSettings"];
    [settingsLoader loadPListDictionary];
    
    //set the array of SettingsOptions as loaded from the plist
    [self setSettingsOptions:[settingsLoader getOweSettingsFromPlist]];
}


@end
