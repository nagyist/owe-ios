//
//  OwesMeViewController.m
//  Owe
//
//  Created by Patrick Farrell on 19/10/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "OwesMeViewController.h"

@interface OwesMeViewController ()

@end

@implementation OwesMeViewController

#pragma mark - LifeCycle Methods

- (id)init
{
    self = [super init];
    if (self)
    {
        //custom initialisation here
        [self setTitle:@"Owes Me"];
    }
    return self;
}


#pragma mark - Instance Methods

- (void) populateTableDataFromDB
{
//    OweDBDAO * dao = [[OweDBDAO sharedInstance] op]
//    
//    OweDAO * dao = [[OweDAO alloc] initWithDBName:@"owe.db" tableName: @"Owe"];
//    
//    [dao openDB];
//    [self setAllOwes:[[NSMutableArray alloc] initWithArray:[dao readAllOwesMeFromDB]]];
//    [dao closeDB];
}



@end
