//
//  NSString+Additions.m
//  Owe
//
//  Created by Patrick Farrell on 10/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

+ (NSString *) createGUIDString
{
    return [[NSUUID UUID] UUIDString];
}

+ (NSString *) localCurrencySymbolString
{
    // TODO: implement this properly to return a proper currency val
    
    return @"£";
}

@end
