//
//  OwesMeViewController.h
//  Owe
//
//  Created by Patrick Farrell on 19/10/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OwesListViewController.h"

@interface OwesMeViewController : OwesListViewController

@end
