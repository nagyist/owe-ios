//
//  OweSettingsViewController.h
//  Owe
//
//  Created by Patrick Farrell on 04/01/2014.
//  Copyright (c) 2014 PatrickFarrellApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OweSettingsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableView_settings;

@end
