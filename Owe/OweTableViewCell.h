//
//  OweTableViewCell.h
//  Owe
//
//  Created by Patrick Farrell on 20/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Owe.h"

@interface OweTableViewCell : UITableViewCell

+ (instancetype) oweCellForTable:(UITableView *) tableView withOwe:(Owe *) owe;

- (void) setupCellWithOwe:(Owe *) owe;

@end
