//
//  NewOweViewController.m
//  Owe
//
//  Created by Patrick Farrell on 02/01/2014.
//  Copyright (c) 2014 PatrickFarrellApps. All rights reserved.
//

#import "NewOweViewController.h"

@interface NewOweViewController ()

@end

@implementation NewOweViewController

#pragma mark - Lifecycle Methods

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Instance Methods

// Configure the View for use
- (void) configureViewForUse
{
    [super configureViewForUse];
    
    //configure the view for a new owe
    [self configureForNewOwe];
}

// Configure the View for a brand new blank Owe
- (void) configureForNewOwe
{
    [self addRightBarButton:@"Save" withAction:@selector (saveOweClicked) isEnabled:NO];
    [self prepareViewForNewOwe];
    [self setUIValuesForNewOwe];
}

// prepare the view for a new owe. Set enabled and visibility etc
- (void) prepareViewForNewOwe
{
    //enable/disable user interactions
    [[self textField_oweAmountTotal] setUserInteractionEnabled:YES];
    [[self textField_oweDescription] setUserInteractionEnabled:YES];
    
    //new owe, make the username textfield first responder and give border a highlight
    [[self textFieldOweUsername] becomeFirstResponder];
    [self displayTextFieldViewActive:[self textFieldOweUsername] isActive:YES];
    
    //new owe, dont have the OweDescription an active border yet. This will be when
    //responder changes to it.
    [self displayTextFieldViewActive:[self textField_oweDescription] isActive:NO];
    
    // dont enable the payments table right now
    [self enablePaymentsTable:NO];
    
    //register a tap gesture on the two textfields (username and description) so we can change border focus
    
    UITapGestureRecognizer * oweDescriptionTapGesture = [[UITapGestureRecognizer alloc]
                                                         initWithTarget:self action:@selector(didTapOweDescriptionTextField:)];
    [oweDescriptionTapGesture setNumberOfTapsRequired:1];
    [[self textField_oweDescription] addGestureRecognizer:oweDescriptionTapGesture];
    
}

// set the values of the UI Elements for the new Owe.
- (void) setUIValuesForNewOwe
{
    // set the title to create
    [self setTitle:@"Create"];
    
    // set date label
    NSString * dateStr = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                        dateStyle:NSDateFormatterShortStyle
                                                        timeStyle:NSDateFormatterNoStyle];
    
    [[self label_dateCreated] setText:dateStr];
    
    // set the amount remaining
    NSString * amountRemainingStr = [[self currencyString] stringByAppendingString:@"----"];
    [[self btn_amountRemaining] setTitle:amountRemainingStr forState:UIControlStateNormal];
    
}

@end
