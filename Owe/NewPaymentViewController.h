//
//  NewPaymentViewController.h
//  Owe
//
//  Created by Patrick Farrell on 12/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Owe.h"
#import "OwePayment.h"

@interface NewPaymentViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) Owe * viewingOwe;

@end
