//
//  OweDBDAO.m
//  Owe
//
//  Created by Patrick Farrell on 08/12/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "OweDBDAO.h"
#import "AppHelperFunctions.h"

@implementation OweDBDAO
@synthesize database, databaseName;


#pragma Singleton Access

// Return singleton object of the OweDAO object
+(OweDBDAO *) sharedInstance
{
    static dispatch_once_t pred;
    static OweDBDAO * shared = nil;
    
    dispatch_once(&pred, ^{
        shared = [[OweDBDAO alloc] init];
    });
    
    return shared;
}

#pragma Instance Methods

// Override Init for customisation
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setDatabaseName:@"owe.db"];
    }
    return self;
}

// Opens the database for interaction
- (void) openDB
{
    [self setDatabase:[[FMDatabaseManager sharedInstance] openDb:[self databaseName]]];
}

// Closes the database for interaction
- (void) closeDB
{
    [[self database] close];
}


#pragma Create

// Creates an Owe in the Owe Table
- (void) createOweInDB:(Owe *) owe
{
    NSString * insert = @"INSERT INTO Owe";
    NSString * values = @"(id, owe_to_username, owe_from_username, amount_total, amount_remain, description, dateCreated) VALUES (?,?,?,?,?,?,?)";
    
    NSString * sql = [insert stringByAppendingString:values];
    
    if([self database] != NULL)
    {
        [[self database] executeUpdate:sql,
         [owe oweID],
         [owe oweToUsername],
         [owe oweFromUsername],
         [NSNumber numberWithFloat:[owe amountTotal]],
         [NSNumber numberWithFloat:[owe amountRemaining]],
         [owe description],
         [AppHelperFunctions shortStyleDateAsString:[owe dateCreated]]];
        
    }
    
}

// Creates an OwePayment in the OwePayment Table
- (void) createOwePaymentInDB:(OwePayment *) owePayment
{
    NSString * insert = @"INSERT INTO OwePayments";
    NSString * values = @"(payment_id, owe_id, amount_paid, date_paid) VALUES (?, ? , ? , ?)";
    
    NSString * sql = [insert stringByAppendingString:values];
    
    [[self database] executeUpdate:sql,
     [owePayment owePaymentID],
     [owePayment oweID],
     [NSNumber numberWithFloat:[owePayment amountPaid]],
     [AppHelperFunctions shortStyleDateAsString:[owePayment datePaid]]];
}


#pragma Read

// Read the Owe By OweID
-(Owe *) readOweByOweID:(NSString *) oweID
{
    Owe * owe = [[Owe alloc] init];
    
    if([self database] != NULL)
    {
        NSString * sql = @"SELECT * FROM Owe WHERE id = ?";
        FMResultSet *results = [[self database] executeQuery:sql, oweID];
        
        while([results next])
        {
            [owe setOweID:[results stringForColumn:@"id"]];
            [owe setOweToUsername:[results stringForColumn:@"owe_to_username"]];
            [owe setOweFromUsername:[results stringForColumn:@"owe_from_username"]];
            [owe setDescription:[results stringForColumn:@"description"]];
            [owe setAmountRemaining:[results doubleForColumn:@"amount_remain"]];
            [owe setAmountTotal:[results doubleForColumn:@"amount_total"]];
            [owe setDateCreated:[AppHelperFunctions shortStyleDateFromString:[results        stringForColumn:@"dateCreated"]]];
        }
        
        [results close];
        [[self database] close];
    }
    
    return owe;
}

// Read all the IOwes. An IOwe is simply an Owe where the 'from' is this users' account
- (NSArray *) readAllIOwesFromDB
{
    NSMutableArray * owes = [[NSMutableArray alloc] init];
    
    if([self database] != NULL)
    {
        NSString * sql = @"SELECT * FROM Owe WHERE owe_from_username = ?";
        NSString * userName = [[PFUser currentUser] username];
        
        FMResultSet *results = [[self database] executeQuery:sql, userName];
        
        while([results next])
        {
            Owe * dbOwe = [[Owe alloc] init];
            
            [dbOwe setOweID:[results stringForColumn:@"id"]];
            [dbOwe setOweToUsername:[results stringForColumn:@"owe_to_username"]];
            [dbOwe setOweFromUsername:[results stringForColumn:@"owe_from_username"]];
            [dbOwe setDescription:[results stringForColumn:@"description"]];
            [dbOwe setAmountRemaining:[results doubleForColumn:@"amount_remain"]];
            [dbOwe setAmountTotal:[results doubleForColumn:@"amount_total"]];
            [dbOwe setDateCreated:[AppHelperFunctions shortStyleDateFromString:[results        stringForColumn:@"dateCreated"]]];
            
            [owes addObject:dbOwe];
        }
        
    }
    
    return [owes copy];
}

// Read all the OwesMe. An OweMe is simply an Owe where the 'to' is this users' account
- (NSArray *) readAllOwesMeFromDB
{
    NSMutableArray * owes = [[NSMutableArray alloc] init];
    
    if([self database] != NULL)
    {
        NSString * sql = @"SELECT * FROM Owe WHERE owe_to_username = ?";
        NSString * userName = [[PFUser currentUser] username];
        
        FMResultSet *results = [[self database] executeQuery:sql, userName];
        
        while([results next])
        {
            Owe * dbOwe = [[Owe alloc] init];
            
            [dbOwe setOweID:[results stringForColumn:@"id"]];
            [dbOwe setOweToUsername:[results stringForColumn:@"owe_to_username"]];
            [dbOwe setOweFromUsername:[results stringForColumn:@"owe_from_username"]];
            //[dbOwe setDateCreatedFromString:[results stringForColumn:@"dateCreated"]];
            [dbOwe setDescription:[results stringForColumn:@"description"]];
            [dbOwe setAmountRemaining:[results doubleForColumn:@"amount_remain"]];
            [dbOwe setAmountTotal:[results doubleForColumn:@"amount_total"]];
            [dbOwe setDateCreated:[AppHelperFunctions shortStyleDateFromString:[results        stringForColumn:@"dateCreated"]]];
            
            [owes addObject:dbOwe];
        }
        
    }
    
    return [owes copy];
}

// Reads database table OwePayment for a payment with a specified paymentID
- (OwePayment *) readOwePaymentByPaymentID:(NSString *) paymentID
{
    OwePayment * dbOwePayment = [[OwePayment alloc] init];
    
    if([self database] != NULL)
    {
        NSString * sql = @"SELECT * FROM OwePayments WHERE payment_id = ?";
        FMResultSet * results = [[self database] executeQuery:sql, paymentID];
        
        while([results next])
        {
            OwePayment * dbOwePayment = [[OwePayment alloc] init];
            
            [dbOwePayment setOwePaymentID:[results stringForColumn:@"payment_id"]];
            [dbOwePayment setOweID:[results stringForColumn:@"owe_id"]];
            [dbOwePayment setAmountPaid:[results doubleForColumn:@"amount_paid"]];
            [dbOwePayment setDatePaid:[AppHelperFunctions shortStyleDateFromString:[results        stringForColumn:@"date_paid"]]];
            
        }
    }
    
    return dbOwePayment;
    
}

// Reads database table OwePayment for all OwePayments with a matching oweID
- (NSArray *) readAllOwePaymentsByOweID:(NSString *) oweID
{
    NSMutableArray * owePayments = [[NSMutableArray alloc] init];
    
    if([self database] != NULL)
    {
        NSString * sql = @"SELECT * FROM OwePayments WHERE owe_id = ? ORDER BY [order_index] DESC";
        FMResultSet *results = [[self database] executeQuery:sql, oweID];
        
        while([results next])
        {
            OwePayment * dbOwePayment = [[OwePayment alloc] init];
            
            [dbOwePayment setOwePaymentID:[results stringForColumn:@"payment_id"]];
            [dbOwePayment setOweID:[results stringForColumn:@"owe_id"]];
            [dbOwePayment setAmountPaid:[results doubleForColumn:@"amount_paid"]];
            [dbOwePayment setDatePaid:[AppHelperFunctions shortStyleDateFromString:[results        stringForColumn:@"date_paid"]]];
            
            [owePayments addObject:dbOwePayment];
        }
    }
    
    return [owePayments copy];
}

// Reads and returns the username in the db User table
- (NSString *) readUser
{
    NSString * usernameInDB = nil;
    
    if([self database] != NULL)
    {
        NSString * sql = @"SELECT * FROM User";
        FMResultSet * results = [[self database] executeQuery:sql];
        
        while([results next])
        {
            usernameInDB = [results stringForColumn:@"username"];
        }
    }
    
    return usernameInDB;
}


#pragma Update

// Updates the amount remaining value in the database for a specific oweID
- (void) updateOwe:(NSString *) oweID withNewAmountRemaining:(float) amountRemaining
{
    if([self database] != NULL)
    {
        [[self database] executeUpdate:@"UPDATE Owe set amount_remain = ? where id = ?", [NSNumber numberWithFloat:amountRemaining], [NSString stringWithFormat:@"%@", oweID], nil];
    }
}


#pragma Delete

// Delete the Owe by ID
- (void) deleteOweWithOweID:(NSString *) oweID
{
    if([self database] != NULL)
    {
        NSString * deleteStatementNS = @"DELETE FROM Owe WHERE id = ?";
        [[self database] executeUpdate:deleteStatementNS,oweID];
    }
}


@end
