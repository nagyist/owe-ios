//
//  OweViewController.h
//  Owe
//
//  Created by Patrick Farrell on 04/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Owe.h"
#import "CustomColorsLoader.h"

@interface OweViewController : UIViewController <UITableViewDelegate,
                                                UITableViewDataSource,
                                                UITextFieldDelegate,
                                                UITextViewDelegate>

@property (nonatomic, strong) Owe * viewingOwe;
@property (nonatomic, strong) NSString * oweID;
@property (strong, nonatomic) NSString * currencyString;
@property (nonatomic) BOOL isAnIOU;
@property (strong, nonatomic) IBOutlet UITextField * textFieldOweUsername;
@property (strong, nonatomic) IBOutlet UILabel *label_dateCreated;
@property (strong, nonatomic) IBOutlet UITextField *textField_oweAmountTotal;
@property (strong, nonatomic) IBOutlet UITextField *textField_oweDescription;
@property (strong, nonatomic) IBOutlet UILabel *label_currency;
@property (strong, nonatomic) IBOutlet UITableView *tableView_payments;
@property (strong, nonatomic) IBOutlet UIButton *btn_amountRemaining;

- (void) refreshViewControllerModelData;
- (void) refreshModelDependentUI;
- (void) configureViewForUse;
- (void) addRightBarButton:(NSString *) title withAction:(SEL) action isEnabled:(BOOL) enabled;
- (void) enablePaymentsTable:(BOOL) enabled;
- (void) displayTextFieldViewActive:(UITextField *) textField isActive:(BOOL) active;
- (Owe *) createOweFromUIValues;
- (void) saveOweClicked;
- (void) didTapOweUsernameTextField:(UITapGestureRecognizer *) gestureRecognizer;
- (void) didTapOweDescriptionTextField:(UITapGestureRecognizer *) gestureRecognizer;
- (void) setAmountRemainingFromAmountTotal;
- (IBAction)amountRemainingClicked:(id)sender;

@end
