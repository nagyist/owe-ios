//
//  SignupLoginViewController.h
//  Owe
//
//  Created by Patrick Farrell on 21/01/2014.
//  Copyright (c) 2014 PatrickFarrellApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupLoginViewController : UIViewController <UITextFieldDelegate>

@end
