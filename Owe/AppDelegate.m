//
//  AppDelegate.m
//  Owe
//
//  Created by Patrick Farrell on 13/10/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "AppDelegate.h"
#import "OweNavigationController.h"
#import "OweTabBarController.h"
#import "FMDatabaseManager.h"
#import "AppTheme.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Override point for customization after application launch.
    //self.window.backgroundColor = [UIColor whiteColor];
    
    //setup Parse
    [Parse setApplicationId:@"DGcH2rl7WxurPEj3AcxT3TKYf0bSk4q1YN1etnK3"
                  clientKey:@"lY3y0codvTEtwV5CIvQ2VfzWN1ny5CCPEFxqkg5T"];
    
    //copy the database into the app if needed
    [[FMDatabaseManager sharedInstance] copyDatabaseIfNeeded:@"owe.db"];
    
    //create the parent TabBarController for app
    //OweTabBarController * tabBarController = [[OweTabBarController alloc] init];
    
    //add completed tab bar controller to the window
    //[[self window] setRootViewController:tabBarController];
    
    //[self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
