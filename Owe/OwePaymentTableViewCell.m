//
//  OwePaymentTableViewCell.m
//  Owe
//
//  Created by Patrick Farrell on 24/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "OwePaymentTableViewCell.h"
#import "NSString+Additions.h"

@interface OwePaymentTableViewCell ()

@property (nonatomic, retain) IBOutlet UILabel * labelOwePaymentDate;
@property (nonatomic, retain) IBOutlet UILabel * labelOwePaymentAmount;

- (void) setupCellWithPayment:(OwePayment *) owePayment;

@end

@implementation OwePaymentTableViewCell

+ (instancetype) owePaymentCellForTable:(UITableView *) tableView withOwePayment:(OwePayment *) payment
{
    //static the reuse id of the cell.
    static NSString * OwePaymentCellIdentifier = @"OwePaymentCell";
    
    OwePaymentTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:OwePaymentCellIdentifier];
    
    if(cell == nil)
    {
        NSArray *nibObjects =
        [[NSBundle mainBundle]loadNibNamed:@"OwePaymentTableCell" owner:nil options:nil];
        
        for (id currentObject in nibObjects)
        {
            if([currentObject isKindOfClass:[OwePaymentTableViewCell class]])
            {
                cell = (OwePaymentTableViewCell *)currentObject;
            }
        }
    }
    
    [cell setupCellWithPayment:payment];
    
    return cell;
}

// Sets the UI Values with the OwePayment model
- (void) setupCellWithPayment:(OwePayment *) owePayment
{
    // set date label
    NSString * dateStr = [NSDateFormatter localizedStringFromDate:[owePayment datePaid]
                                                        dateStyle:NSDateFormatterShortStyle
                                                        timeStyle:NSDateFormatterNoStyle];
    
    [[self labelOwePaymentDate] setText:dateStr];
    
    
    float amountPaid = [owePayment amountPaid];
    NSString* paidValueStr = [NSString stringWithFormat:@"%.02f", amountPaid];
    NSString * currency = [NSString localCurrencySymbolString];
    NSString * amountPaidStr = [currency stringByAppendingString:paidValueStr];
    [[self labelOwePaymentAmount] setText:amountPaidStr];
}

@end
