//
//  OweTableViewCell.m
//  Owe
//
//  Created by Patrick Farrell on 20/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "OweTableViewCell.h"
#import "AppHelperFunctions.h"
#import "NSString+Additions.h"
#import "CustomColorsLoader.h"

@interface OweTableViewCell ()

@property (nonatomic, retain) IBOutlet UILabel * labelOweName;
@property (nonatomic, retain) IBOutlet UILabel * labelOweDateCreated;
@property (nonatomic, retain) IBOutlet UILabel * labelOweAmountTotal;
@property (nonatomic, retain) IBOutlet UITextView * textViewOweDescription;

- (void) customiseUI;

@end


@implementation OweTableViewCell

#pragma mark - Class Methods

+ (instancetype) oweCellForTable:(UITableView *) tableView withOwe:(Owe *) owe
{
    //static the reuse id of the cell.
    static NSString * OweCellIdentifier = @"OweTableCell";
    
//    OweTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:OweCellIdentifier];
//    
//    if(cell == nil)
//    {
//        NSArray *nibObjects = [[NSBundle mainBundle]loadNibNamed:@"OweTableCell" owner:nil options:nil];
//        
//        for (id currentObject in nibObjects)
//        {
//            if([currentObject isKindOfClass:[OweTableViewCell class]])
//            {
//                cell = (OweTableViewCell *)currentObject;
//            }
//        }
//    }
    
    OweTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:OweCellIdentifier];
    

    //setup the cell with the Owe
    [cell setupCellWithOwe:owe];
    
    return cell;
}

#pragma mark - Instance Methods

// Setup the cell with data
- (void) setupCellWithOwe:(Owe *) owe
{
    [[self labelOweName] setText:[owe oweToUsername]];
    [[self labelOweDateCreated] setText:[AppHelperFunctions shortStyleDateAsString:[owe dateCreated]]];
    [[self textViewOweDescription] setText:[owe description]];
    
    // set the amount remaining
    float amountTotal = [owe amountTotal];
    NSString* amountValueStr = [NSString stringWithFormat:@"%.02f", amountTotal];
    NSString * currency = [NSString localCurrencySymbolString];
    NSString * amountTotalStr = [currency stringByAppendingString:amountValueStr];

    [[self labelOweAmountTotal] setText:amountTotalStr];
    
    [self customiseUI];
}

// Customise the look and feel of the UI of the cell
- (void) customiseUI
{
    /*
     
     The OweTableCell has a view inside the main views that are
     used as background and selectedBackgroundView
     
     We will get the topmost subview of the backgroundView and selectedBackgroundView
     and apply colors and borders to them
     
     */
    
    //create a colors loader object
    CustomColorsLoader * colorLoader = [[CustomColorsLoader alloc] initWithPlist:@"CustomColors"];
    
    //load the plist
    [colorLoader loadPListDictionary];
    
    
//    //get our colors
//    UIColor * cellDefaultColor = [colorLoader customColorWithName:@"OweCellBackground" withAlpha:1.0f];
//    
//    UIColor * cellSelectedColor = [colorLoader customColorWithName:@"OweCellSelectedBackground" withAlpha:1.0f];
//    
//    UIColor * borderColor = [colorLoader customColorWithName:@"OweCellBorder" withAlpha:1.0f];
    
//    //setup topmost view in backgroundView
//    UIView * backgroundViewTop = [[[self backgroundView] subviews] objectAtIndex:0];
//    [backgroundViewTop setBackgroundColor:cellDefaultColor];
//    [[backgroundViewTop layer] setMasksToBounds:YES];
//    [[backgroundViewTop layer] setCornerRadius:2.0f];
//    [[backgroundViewTop layer] setBorderWidth:1.0f];
//    [[backgroundViewTop layer] setBorderColor:[borderColor CGColor]];
//    
//    //setup topmost view in selectedBackgroundView
//    UIView * selectedBackgroundViewTop = [[[self selectedBackgroundView] subviews] objectAtIndex:0];
//    [selectedBackgroundViewTop setBackgroundColor:cellSelectedColor];
//    [[selectedBackgroundViewTop layer] setMasksToBounds:YES];
//    [[selectedBackgroundViewTop layer] setCornerRadius:2.0f];
//    [[selectedBackgroundViewTop layer] setBorderWidth:1.0f];
//    [[selectedBackgroundViewTop layer] setBorderColor:[borderColor CGColor]];
}



@end
