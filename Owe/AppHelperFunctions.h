//
//  AppHelperFunctions.h
//  Owe
//
//  Created by Patrick Farrell on 08/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

// Description : Some Helper Functions that all projects might find useful

#import <Foundation/Foundation.h>

@interface AppHelperFunctions : NSObject

+ (NSDate *) shortStyleDateFromString: (NSString *) dateAsString;
+ (NSString *) shortStyleDateAsString: (NSDate *) dateToBeString;


@end
