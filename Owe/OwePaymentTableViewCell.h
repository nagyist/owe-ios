//
//  OwePaymentTableViewCell.h
//  Owe
//
//  Created by Patrick Farrell on 24/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OwePayment.h"

@interface OwePaymentTableViewCell : UITableViewCell

+ (instancetype) owePaymentCellForTable:(UITableView *) tableView withOwePayment:(OwePayment *) payment;



@end
