//
//  OweOptionsOperations.h
//  Owe
//
//  Created by Patrick Farrell on 17/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OweOptionsOperations <NSObject>

@required
- (void) didDismissOptionsView;

@end
