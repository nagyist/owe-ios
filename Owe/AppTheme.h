//
//  AppTheme.h
//  Owe
//
//  Created by Patrick Farrell on 15/01/2014.
//  Copyright (c) 2014 PatrickFarrellApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppTheme : NSObject

+(instancetype) sharedInstance;

@property (nonatomic, strong, readonly) UIColor * appThemeColor;

@end
