//
//  OweTabBarController.m
//  Owe
//
//  Created by Patrick Farrell on 30/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "OweTabBarController.h"
#import "IOweViewController.h"
#import "OwesMeViewController.h"
#import "OweNavigationController.h"
#import  "AppTheme.h"


@implementation OweTabBarController

- (id) init
{
    self = [super init];
    
    if(self)
    {
        [self customiseTabBarController];
    }
    
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return [self.selectedViewController supportedInterfaceOrientations];
}

-(BOOL)shouldAutorotate
{
    return NO;
}

// Customise the TabBarController
- (void) customiseTabBarController
{
    // apply custom themes etc
    [[UITabBar appearance] setBackgroundColor:[[AppTheme sharedInstance] appThemeColor]];
    
    //create the view controllers that this tabbarcontroller will use
    [self createAppViewControllers];
}

// Creates the view controllers that this app has
- (void) createAppViewControllers
{
    //create pointers for view controllers
    UIViewController * iOweViewController = [[IOweViewController alloc] init];
    UIViewController * owesMeViewController = [[OwesMeViewController alloc] init];
    
    //add each to their own nav controller
    OweNavigationController * iOweNavController = [[OweNavigationController alloc] initWithRootViewController:iOweViewController];
    OweNavigationController * owesMeNavController = [[OweNavigationController alloc] initWithRootViewController:owesMeViewController];
    
    //create array of these nav controller to be submitted to the tab bar controller
    NSArray * tabBarViewControllerArray = [[NSArray alloc] initWithObjects:iOweNavController,owesMeNavController, nil];
    
    //add view controllers to this tab bar
    [self setViewControllers:tabBarViewControllerArray];
}


@end
