//
//  OweTabBarController.h
//  Owe
//
//  Created by Patrick Farrell on 30/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppTheme.h"

@interface OweTabBarController : UITabBarController


@end
