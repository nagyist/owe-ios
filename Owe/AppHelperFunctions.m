//
//  AppHelperFunctions.m
//  Owe
//
//  Created by Patrick Farrell on 08/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "AppHelperFunctions.h"

@implementation AppHelperFunctions

#pragma mark Class Methods


+ (NSDate *) shortStyleDateFromString: (NSString *) dateAsString
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateStyle: NSDateFormatterShortStyle];
    [dateFormat setTimeStyle:NSDateFormatterNoStyle];
    
    return [dateFormat dateFromString:dateAsString];
}

+ (NSString *) shortStyleDateAsString: (NSDate *) dateToBeString
{
    NSString * dateStr = [NSDateFormatter localizedStringFromDate:dateToBeString
                                                        dateStyle:NSDateFormatterShortStyle
                                                        timeStyle:NSDateFormatterNoStyle];
    
    return dateStr;
}

@end
