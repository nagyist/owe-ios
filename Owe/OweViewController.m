//
//  OweViewController.m
//  Owe
//
//  Created by Patrick Farrell on 04/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "OweViewController.h"
#import "AppHelperFunctions.h"
#import "NSString+Additions.h"
#import "OwePayment.h"
#import "OweDBDAO.h"
#import "NewPaymentViewController.h"
#import "OwePaymentTableViewCell.h"


@interface OweViewController ()

@property (nonatomic, strong) CustomColorsLoader * customColorLoader;
@property (strong, nonatomic) UIBarButtonItem * rightBarButtonItem;


@end

@implementation OweViewController

#pragma mark - LifeCycle Methods

- (id)init
{
    self = [super initWithNibName:@"OweViewController" bundle:nil];
    if (self)
    {
        // get currency symbol
        [self setCurrencyString:[NSString localCurrencySymbolString]];
        
        //create the custom colors loader object
        [self setCustomColorLoader:[[CustomColorsLoader alloc] initWithPlist:@"CustomColors"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set delegate and datasource of payments table
    [[self tableView_payments] setDelegate:self];
    [[self tableView_payments] setDataSource:self];
    
    //set delegates of textfields and textviews
    [[self textField_oweAmountTotal] setDelegate:self];
    [[self textField_oweDescription] setDelegate:self];
}

- (void) viewWillAppear:(BOOL)animated
{
    if ([self oweID] != nil)
    {
        //set the viewingOwe property by the oweID
        OweDBDAO * dao = [OweDBDAO sharedInstance];
        [dao openDB];
        [self setViewingOwe:[dao readOweByOweID:[self oweID]]];
        [dao closeDB];
    }
    else
        [self setViewingOwe:nil];
    
    //perform customisations to the view when it loads
    [self configureViewForUse];
    
    
    //refresh the view
    if([self viewingOwe] != NULL)
    {
        [self refreshViewControllerModelData];
        [self refreshModelDependentUI];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Methods

// Return number of rows in table
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self viewingOwe] != NULL)
    {
       return [[[self viewingOwe] owePayments] count];
    }
    else
        return 0;
    
}

// Return the Cell at the given index path
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //get the OwePayment at this index and set up the contents of the cell with it
    OwePayment * owePayment = [[[self viewingOwe] owePayments] objectAtIndex:[indexPath row]];
    
    OwePaymentTableViewCell * cell = [OwePaymentTableViewCell owePaymentCellForTable:tableView withOwePayment:owePayment];
    
    return cell;
}

// Do something when row is selected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}


#pragma mark - UITextField Methods

// Called when editing did begin on a UITextField (AmountTotal field)
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == [self textField_oweAmountTotal])
    {
        //when the amount total is being edited,
        //change the right bar button item to say ok and change its target
        
        //wipe the text clear
        [[self textField_oweAmountTotal] setText:@""];
    }
}

// Called when text is being changed in the UITextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //check editing the AmountTotal field
    if(textField == [self textField_oweAmountTotal])
    {
        //once starts editing, allow the save button in nav bar
        //dont allow save button if textfield is empty
        NSString * amountTotalString = [[self textField_oweAmountTotal] text];
        
        if([amountTotalString length] != 0 )
        {
            [[self rightBarButtonItem] setEnabled:YES];
            //[self setAmountRemainingFromAmountTotal];
        }
        
        
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
    
        if ([arrayOfString count] > 2 )
            return NO;

        return YES;
    }
    else
        return YES;

}

// Called when the textfield has finished editing and will resign responder
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //copy over the amount total to the amount remaining button
    if(textField == [self textField_oweAmountTotal])
    {
        [self setAmountRemainingFromAmountTotal];
    }
}

// Called when the TextField should resign its responder (Return/Done pressed on keyboard)
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    BOOL returnVal = NO;
    
    if(textField == [self textFieldOweUsername])
    {
        //pass keyboard focus onto the OweDescription text field
        returnVal = [[self textField_oweDescription] becomeFirstResponder];
        
        //change the border of the textFieldOweUsername to not active
        [self displayTextFieldViewActive:textField isActive:NO];
        
        //we are changing the responder to the textField_oweDescription so make its border active
        [self displayTextFieldViewActive:[self textField_oweDescription] isActive:YES];
        
    }
    else if (textField == [self textField_oweDescription])
    {
        //pass keyboard focus onto the AmountTotal text field
        returnVal = [[self textField_oweAmountTotal] becomeFirstResponder];
        
        //remove activation of border on the textField_oweDescription now
        [self displayTextFieldViewActive:[self textField_oweDescription] isActive:NO];
        
    }
    else
        //resign responder
        returnVal = [textField resignFirstResponder];
    
    return returnVal;
}


#pragma mark - Instance Methods

// Refresh all modal data that this view controller will need
- (void) refreshViewControllerModelData
{
    //refresh the owe payments for the viewing owe again
    [[self viewingOwe] populateOwePayments];
    
    //reload the payments table
    [[self tableView_payments] reloadData];
}

// Refresh any UI Elements that use Model Data
- (void) refreshModelDependentUI
{
   
}

- (void) addRightBarButton:(NSString *) title withAction:(SEL) action isEnabled:(BOOL) enabled;
{
    [self setRightBarButtonItem:[[UIBarButtonItem alloc] init]];
    [[self rightBarButtonItem] setAction:action];
    [[self rightBarButtonItem] setEnabled:enabled];
    [[self rightBarButtonItem] setTarget:self];
    [[self rightBarButtonItem] setTitle:title];
    
    // then we add the button to the navigation bar
    [[self navigationItem] setRightBarButtonItem:[self rightBarButtonItem]];
}

// Configure the View for use
- (void) configureViewForUse
{
    // depending if there is a owe to view or not
    // configure the view.
    
    // basic / common view setup
    [[self label_currency] setText:[self currencyString]];
    
    //create a colors loader object
    
    //load the plist
    [[self customColorLoader] loadPListDictionary];
    
    UIColor * amountTextColor = [[self customColorLoader] customColorWithName:@"OweCellSelectedBackground"
                                                       withAlpha:1.0f];
    
    [[self btn_amountRemaining] setTitleColor:amountTextColor forState:UIControlStateNormal];
    [[self textField_oweAmountTotal] setAdjustsFontSizeToFitWidth:YES];
    [[self textFieldOweUsername] setAdjustsFontSizeToFitWidth:YES];
    [[self textField_oweDescription] setAdjustsFontSizeToFitWidth:YES];
    
    
    
}

// Create an Owe object from values in the UI elements -- Only going to be called for a new Owe creation
- (Owe *) createOweFromUIValues
{
    Owe * newOwe = [[Owe alloc] init];
    
    NSString * currentUsername = [[PFUser currentUser] username];
    CGFloat initialAmount = 0.0f;
    
    //get the amount in the owe_total textview as a float
    initialAmount = [[[self textField_oweAmountTotal] text] floatValue];
    
    [newOwe setOweID:[NSString createGUIDString]];
    [newOwe setOweFromUsername:currentUsername];
    [newOwe setOweToUsername:[[self textFieldOweUsername] text]];
    [newOwe setAmountTotal:initialAmount];
    [newOwe setAmountRemaining:initialAmount];
    [newOwe setDescription:[[self textField_oweDescription] text]];
    [newOwe setDateCreated:[NSDate date]];
    
    return newOwe;
}

// Save the new Owe to the database
- (void) saveOweClicked
{
    //create the Owe from the UI Elements
    Owe * newOwe = [self createOweFromUIValues];
    
    //use the OweDAO to save the newOwe to the database
    OweDBDAO * dao = [OweDBDAO sharedInstance];
    [dao openDB];
    [dao createOweInDB:newOwe];
    [dao closeDB];
    
    //once saved, pop this view controller off the navigation stack
    [[self navigationController] popViewControllerAnimated:YES];
}

// Enable the payments table to allow interaction
- (void) enablePaymentsTable:(BOOL) enabled
{
    // The payments button will reflect the state of the payments table
    [[self btn_amountRemaining] setUserInteractionEnabled:enabled];
    
    [[self tableView_payments] setUserInteractionEnabled:enabled];
    
    if(enabled == true)
        [[self tableView_payments] setAlpha:1.0f];
    else
        [[self tableView_payments] setAlpha:0.0f];
}

// Changes the TextField's appearance based on active YES/NO.
- (void) displayTextFieldViewActive:(UITextField *) textField isActive:(BOOL) active
{
    UIColor * borderColor;
    
    if(active)
        borderColor = [[self customColorLoader] customColorWithName:@"TextFieldActiveBorder"
                                                          withAlpha:1.0f];
    else
       borderColor = [[self customColorLoader] customColorWithName:@"TextFieldDefaultBorder"
                                                         withAlpha:1.0f];
    
    [[textField layer] setMasksToBounds:YES];
    [[textField layer] setCornerRadius:4.0f];
    [[textField layer] setBorderWidth:1.0f];
    [[textField layer] setBorderColor:[borderColor CGColor]];
    
}

// Sets the text of the amount remaining button to be the same as the value in the amount total textfield
- (void) setAmountRemainingFromAmountTotal
{
    // build string for the amount remaining button
    NSString * amountRemainingStr = [[NSString localCurrencySymbolString] stringByAppendingString:[[self textField_oweAmountTotal] text]];
    
    [[self btn_amountRemaining] setTitle:amountRemainingStr forState:UIControlStateNormal];
}

// Called when the user taps on the OweUsername textfield
- (void) didTapOweUsernameTextField:(UITapGestureRecognizer *) gestureRecognizer
{
    //tapped the OweUsername so de-activate the border on the OweDescription
    [self displayTextFieldViewActive:[self textField_oweDescription] isActive:NO];
    
    //activate border on the OweUsername
    [self displayTextFieldViewActive:[self textFieldOweUsername] isActive:YES];
    
    //change keyboard focus to the OweUsername field
    [[self textFieldOweUsername] becomeFirstResponder];
    
    UITapGestureRecognizer * oweDescriptionTapGesture = [[UITapGestureRecognizer alloc]
                                                         initWithTarget:self action:@selector(didTapOweDescriptionTextField:)];
    [oweDescriptionTapGesture setNumberOfTapsRequired:1];
    [[self textField_oweDescription] addGestureRecognizer:oweDescriptionTapGesture];
}

// Called when the user taps on the OweDescription textfield
- (void) didTapOweDescriptionTextField:(UITapGestureRecognizer *) gestureRecognizer
{
    //tapped the OweDescription so de-activate the border on the OweUsername
    [self displayTextFieldViewActive:[self textFieldOweUsername] isActive:NO];
    
    //activate border on the OweDescription
    [self displayTextFieldViewActive:[self textField_oweDescription] isActive:YES];
    
    //change keyboard focus to the OweDescription field
    [[self textField_oweDescription] becomeFirstResponder];
    
    UITapGestureRecognizer * oweUsernameTapGesture = [[UITapGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(didTapOweUsernameTextField:)];
    [oweUsernameTapGesture setNumberOfTapsRequired:1];
    [[self textFieldOweUsername] addGestureRecognizer:oweUsernameTapGesture];
    
}

// The amount remaining button click event. Displays the Payments Modal ViewController.
- (IBAction)amountRemainingClicked:(id)sender
{
    // open new payment dialog
    NewPaymentViewController * newPaymentViewController = [[NewPaymentViewController alloc] init];
    [newPaymentViewController setViewingOwe:[self viewingOwe]];
    
    UINavigationController * modalNavController = [[UINavigationController alloc] initWithRootViewController:newPaymentViewController];
    
    [newPaymentViewController setModalInPopover:YES];
    [newPaymentViewController setModalPresentationStyle:UIModalPresentationNone];
    
    [self presentViewController:modalNavController animated:YES completion:Nil];
}


@end
