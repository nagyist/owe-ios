//
//  IOweViewController.h
//  Owe
//
//  Created by Patrick Farrell on 19/10/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabaseManager.h"
#import "OweDBDAO.h"
#import "Owe.h"
#import "OweOptionsOperations.h"

@interface OwesListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableView_owes;
@property (strong, nonatomic) NSMutableArray * allOwes;

- (void) populateTableDataFromDB;

@end
