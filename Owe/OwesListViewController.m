//
//  IOweViewController.m
//  Owe
//
//  Created by Patrick Farrell on 19/10/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "OwesListViewController.h"


#import "NewOweViewController.h"
#import "ExistingOweViewController.h"
#import "OweSettingsViewController.h"
#import "OweTableViewCell.h"
#import "CustomColorsLoader.h"
#import "SignupLoginViewController.h"



@interface OweViewController ()

@property (nonatomic) BOOL oweOptionsVisible;

- (void) addRightBarButtonItem;
- (void) addLeftBarButtonItem;
- (void) rightBarButtonItemPressed;
- (void) leftBarButtonItemPressed;
- (void) checkUserLogin;
- (void) requestUserloginSignUp;

@end

@implementation OwesListViewController

#pragma mark - LifeCycle Methods

- (id)init
{
    self = [super initWithNibName:@"OwesListViewController" bundle:nil];
    if (self)
    {
        [self addRightBarButtonItem];
        [self addLeftBarButtonItem];
    }
    return self;
} 

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self tableView_owes] setDelegate:self];
    [[self tableView_owes] setDataSource:self];
    
    
}

- (void) viewDidAppear:(BOOL)animated
{
    //check user logged into app
    [self checkUserLogin];
    
    // populate the data of IOwes
    [self populateTableDataFromDB];
    [[self tableView_owes] reloadData];
}


#pragma mark - Instance Methods

// Checks that the user is logged in or signed up and if not presents the login/signup VC
- (void) checkUserLogin
{
    //check the current user
    PFUser *currentUser = [PFUser currentUser];
    
    if (!currentUser)
    {
        //show the signup or login screen
        [self requestUserloginSignUp];
    }
}

// Presents the SignUp/Login VC to the user
- (void) requestUserloginSignUp
{
    //create signup/login vc
    SignupLoginViewController * signupLoginViewController = [[SignupLoginViewController alloc] init];
    [signupLoginViewController setModalPresentationStyle:UIModalPresentationFormSheet];
    
    [self presentViewController:signupLoginViewController animated:YES completion:nil];
}

// Adds a Button to the RHS of the Navigation Bar
- (void) addRightBarButtonItem
{
    UIBarButtonItem * addNewOweButton = [[UIBarButtonItem alloc]init];
    [addNewOweButton setAction:@selector(rightBarButtonItemPressed)];
    [addNewOweButton setTitle:@"Add"];
    [addNewOweButton setTarget:self];
    
    // then we add the button to the navigation bar
    [[self navigationItem] setRightBarButtonItem: addNewOweButton];

}

// Adds a Button to the LHS of the Navigation Bar
- (void) addLeftBarButtonItem
{
    UIBarButtonItem * appSettingsButton = [[UIBarButtonItem alloc]init];
    [appSettingsButton setAction:@selector(leftBarButtonItemPressed)];
    [appSettingsButton setTitle:@"Sett"];
    [appSettingsButton setTarget:self];
    
    // then we add the button to the navigation bar
    [[self navigationItem] setLeftBarButtonItem:appSettingsButton];
    
}

// Called when the RHS Bar Button Item is pressed
- (void) rightBarButtonItemPressed
{
    //create an ExistingOweViewController
    OweViewController * newOweViewController = [[NewOweViewController alloc] init];
    
    //set the oweID property to nil
    [newOweViewController setOweID:nil];
    
    //push the vc onto navigation stack
    [[self navigationController] pushViewController:newOweViewController animated:YES];
}

// Called when the LHS Bar Button Item is pressed
- (void) leftBarButtonItemPressed
{
    //create an ExistingOweViewController
    OweSettingsViewController * oweSettingsViewController = [[OweSettingsViewController alloc] init];
    
    //want it to have a navigation bar for completness sake
    UINavigationController * modalNavController = [[UINavigationController alloc] initWithRootViewController:oweSettingsViewController];
    
    [oweSettingsViewController setModalInPopover:YES];
    [oweSettingsViewController setModalPresentationStyle:UIModalPresentationNone];
    
    [self presentViewController:modalNavController animated:YES completion:Nil];
}

- (void) populateTableDataFromDB
{
    // this method is overridden in the subclasses
}

#pragma mark - UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = [[self allOwes] count];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //get the owe at this index and set up the contents of the cell with it
    Owe * owe = [[self allOwes] objectAtIndex:[indexPath row]];
    
    OweTableViewCell * cell = [OweTableViewCell oweCellForTable:tableView withOwe:owe];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    //create an ExistingOweViewController
//    OweViewController * existingOweViewController = [[ExistingOweViewController alloc] init];
//    
//    //get the owe at this index
//    Owe * owe = [[self allOwes] objectAtIndex:[indexPath row]];
//    
//    //set the oweID property
//    [existingOweViewController setOweID:[owe oweID]];
//    
//    //push the vc onto navigation stack
//    [[self navigationController] pushViewController:existingOweViewController animated:YES];
}


#pragma mark - Storyboard Methods

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"OweDetailSegue"])
    {
        //UINavigationController *navigationController = segue.destinationViewController;
        ExistingOweViewController * existingOweViewController = segue.destinationViewController;
        Owe * owe = [[self allOwes] objectAtIndex:[[[self tableView_owes] indexPathForSelectedRow] row]];
        [existingOweViewController setOweID:[owe oweID]];
    }
}



@end


