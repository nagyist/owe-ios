//
//  OweSetting.h
//  Owe
//
//  Created by Patrick Farrell on 05/01/2014.
//  Copyright (c) 2014 PatrickFarrellApps. All rights reserved.
//

#import "PListLoader.h"

@interface OweSetting : PListLoader

@property (strong, nonatomic) NSString * settingTitle;
@property (strong, nonatomic) NSString * settingTarget;
@property (strong, nonatomic) NSString * settingIconName;


@end
