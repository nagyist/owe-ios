//
//  OweNavigationController.m
//  Owe
//
//  Created by Patrick Farrell on 30/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "OweNavigationController.h"
#import "AppTheme.h"

@interface OweNavigationController ()

@property (nonatomic, strong) AppTheme * appTheme;

@end

@implementation OweNavigationController

- (id) initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    
    if(self)
    {
        [self customiseNavigationBar];
    }
    
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return [self.topViewController supportedInterfaceOrientations];
}

-(BOOL)shouldAutorotate
{
    return NO;
}

// Customise the NavigationBar
- (void) customiseNavigationBar
{
    // nav bar color
    [[UINavigationBar appearance] setBarTintColor:[[AppTheme sharedInstance] appThemeColor]];
    
    //nav bar title
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4];
    shadow.shadowOffset = CGSizeMake(0, 1);
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          [UIColor darkTextColor],
                                                          NSForegroundColorAttributeName,
                                                          [UIFont fontWithName:@"Helvetica-Neue"
                                                                          size:21.0],NSFontAttributeName, nil]];
}


@end






