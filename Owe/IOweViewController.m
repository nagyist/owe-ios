//
//  IOweViewController.m
//  Owe
//
//  Created by Patrick Farrell on 26/10/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "IOweViewController.h"

@interface IOweViewController ()

@end

@implementation IOweViewController

#pragma mark - Lifecycle Methods

- (id)init
{
    self = [super init];
    if (self)
    {
        //custom initialisation here
        [self setTitle:@"I Owe"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

#pragma mark - Instance Methods

// Reads a list of IOwes from the database
-(void) populateTableDataFromDB
{
    OweDBDAO * dao = [OweDBDAO sharedInstance];
    
    [dao openDB];
    [self setAllOwes:[[NSMutableArray alloc] initWithArray:[dao readAllIOwesFromDB]]];
    [dao closeDB];
}

@end
