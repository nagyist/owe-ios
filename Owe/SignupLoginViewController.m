//
//  SignupLoginViewController.m
//  Owe
//
//  Created by Patrick Farrell on 21/01/2014.
//  Copyright (c) 2014 PatrickFarrellApps. All rights reserved.
//

#import "SignupLoginViewController.h"
#import <Parse/PFUser.h>

@interface SignupLoginViewController ()

@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;

- (IBAction)signUpLoginSelected:(id)sender;
- (IBAction)cancelSignUpLoginSelected:(id)sender;
- (void) signUpUser;

@end

@implementation SignupLoginViewController

- (id)init
{
    self = [super initWithNibName:Nil bundle:nil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    //change keyboard focus to the Username field
    [[self usernameTextField] becomeFirstResponder];
}

#pragma mark - Instance Methods

// Closes the VC
- (void) dismissSignupViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Called when the user presses the Signup/Login button
- (IBAction)signUpLoginSelected:(id)sender
{
    [self signUpUser];
}

// Called when the user chooses to close the Signup/Login scene.
- (IBAction)cancelSignUpLoginSelected:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) signUpUser
{
    //create PFUser object from UITextFields
    PFUser * user = [PFUser user];
    [user setUsername:[[self usernameTextField] text]];
    [user setPassword:[[self passwordTextField] text]];
    [user setEmail:[[self emailTextField] text]];
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
    {
        if (!error)
        {
            [self dismissSignupViewController];
        }
        else
        {
            NSString * errorString = [error userInfo][@"error"];
            
            // Show the errorString somewhere and let the user try again.
            UIAlertView * message = [[UIAlertView alloc] initWithTitle:@"Signup Error!"
                                                              message:errorString
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];
        }
    }];
}

// Called when the TextField should resign its responder (Return/Done pressed on keyboard)
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    BOOL returnVal = NO;
    
    if(textField == [self usernameTextField])
    {
        //pass keyboard focus onto the OweDescription text field
        returnVal = [[self passwordTextField] becomeFirstResponder];
    }
    else if (textField == [self passwordTextField])
    {
        //pass keyboard focus onto the AmountTotal text field
        returnVal = [[self emailTextField] becomeFirstResponder];
    }
    else
    {
        //resign responder
        returnVal = [textField resignFirstResponder];
    }
    
    return returnVal;
}


@end
