//
//  OwePayment.m
//  Owe
//
//  Created by Patrick Farrell on 10/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "OwePayment.h"
#import "NSString+Additions.h"

@implementation OwePayment
@synthesize owePaymentID, oweID, datePaid, amountPaid;

//- (void) createOwePaymentUniqueID
//{
//    [self setOweID:[NSString createGUIDString]];
//}


@end
