//
//  AppTheme.m
//  Owe
//
//  Created by Patrick Farrell on 15/01/2014.
//  Copyright (c) 2014 PatrickFarrellApps. All rights reserved.
//

#import "AppTheme.h"
#import "CustomColorsLoader.h"

@interface AppTheme ()

- (void) loadColors;

@end


@implementation AppTheme

+(instancetype) sharedInstance
{
    static dispatch_once_t pred;
    static AppTheme * shared = nil;
    
    dispatch_once(&pred, ^{
        shared = [[AppTheme alloc] init];
        [shared loadColors];
    });
    
    return shared;
}

// Load the colors that represent the application theme
- (void) loadColors
{
    CustomColorsLoader * colorLoader = [[CustomColorsLoader alloc] initWithPlist:@"CustomColors"];
    [colorLoader loadPListDictionary];
    
    //set the app theme color
    _appThemeColor = [colorLoader customColorWithName:@"OweAppTheme" withAlpha:1.0f];
    
}


@end
