//
//  OweDBDAO.h
//  Owe
//
//  Created by Patrick Farrell on 08/12/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabaseManager.h"
#import "Owe.h"
#import "OwePayment.h"

@interface OweDBDAO : NSObject

// **** Members **** //

@property (nonatomic, strong) FMDatabase * database;
@property (nonatomic, strong) NSString * databaseName;


// **** Methods **** //

+ (id) sharedInstance;

- (void) openDB;
- (void) closeDB;

- (void) createOweInDB:(Owe *) owe;
- (void) createOwePaymentInDB:(OwePayment *) owePayment;

- (NSArray *) readAllIOwesFromDB;
- (NSArray *) readAllOwesMeFromDB;
- (Owe *) readOweByOweID:(NSString *) oweID;
- (NSArray *) readAllOwePaymentsByOweID:(NSString *) oweID;
- (OwePayment *) readOwePaymentByPaymentID:(NSString *) paymentID;
- (NSString *) readUser;

- (void) updateOwe:(NSString *) oweID withNewAmountRemaining:(float) amountRemaining;
- (void) deleteOweWithOweID:(NSString *) oweID;


@end
