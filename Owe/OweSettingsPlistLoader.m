//
//  OweSettingsLoader.m
//  Owe
//
//  Created by Patrick Farrell on 05/01/2014.
//  Copyright (c) 2014 PatrickFarrellApps. All rights reserved.
//

#import "OweSettingsPlistLoader.h"

@implementation OweSettingsPlistLoader


// Reads the Plist for the OweSettings and returns array of results.
- (NSArray *) getOweSettingsFromPlist
{
    //array to return
    NSMutableArray * returningArray = [[NSMutableArray alloc] init];
    
    //get the array of settings from the plist
    NSArray * settings = [[self pListDictionary] objectForKey:@"Settings"];
    
    //loop through the array
    for (NSDictionary * settingDict in settings)
    {
        OweSetting * setting = [[OweSetting alloc] init];
        [setting setSettingTitle:[settingDict objectForKey:@"Title"]];
        [setting setSettingTarget:[settingDict objectForKey:@"Target"]];
        [setting setSettingIconName:[settingDict objectForKey:@"IconName"]];
        
        [returningArray addObject:setting];
    }
    
    return [returningArray copy];
}

@end
