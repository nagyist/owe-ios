//
//  NSString+Additions.h
//  Owe
//
//  Created by Patrick Farrell on 10/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

+ (NSString *) createGUIDString;
+ (NSString *) localCurrencySymbolString;

@end
