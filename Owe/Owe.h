//
//  Owe.h
//  Owe
//
//  Created by Patrick Farrell on 19/10/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Owe : NSObject

@property (nonatomic) NSString * oweID;
@property (nonatomic) NSString * oweToUsername;
@property (nonatomic) NSString * oweFromUsername;
@property (nonatomic) float  amountTotal;
@property (nonatomic) float  amountRemaining;
@property (nonatomic) NSString * description;
@property (nonatomic) NSDate * dateCreated;
@property (nonatomic, strong) NSArray * owePayments;

//- (void) createOweUniqueID;
- (void) populateOwePayments;

@end
