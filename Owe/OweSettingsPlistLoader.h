//
//  OweSettingsLoader.h
//  Owe
//
//  Created by Patrick Farrell on 05/01/2014.
//  Copyright (c) 2014 PatrickFarrellApps. All rights reserved.
//

#import "PListLoader.h"
#import "OweSetting.h"

@interface OweSettingsPlistLoader : PListLoader

@property (strong, nonatomic) NSArray * plistSettings;

- (NSArray *) getOweSettingsFromPlist;

@end
