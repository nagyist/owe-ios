//
//  ExistingOweViewController.m
//  Owe
//
//  Created by Patrick Farrell on 02/01/2014.
//  Copyright (c) 2014 PatrickFarrellApps. All rights reserved.
//

#import "ExistingOweViewController.h"

@interface ExistingOweViewController ()

@end

@implementation ExistingOweViewController

#pragma mark - Lifecycle Methods

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

#pragma mark - Instance Methods

// Configure the View for a brand new blank Owe
- (void) configureViewForUse
{
    [super configureViewForUse];
    
    //configure the view for an existing owe
    [self configureForExistingOwe];
}

// Configure the View for an existing Owe that user is viewing or is making a payment
- (void) configureForExistingOwe
{
    // decide if an IOU or YOM - check based on 'from username' = current user.username
    NSString * currentUserName = [[PFUser currentUser] username];
    
    if([currentUserName isEqualToString:[[self viewingOwe] oweFromUsername]])
        [self setIsAnIOU:YES];
    
    [self prepareViewForExistingOwe];
    [self setUIValuesForExistingOwe];
}

// prepare the view for an existing owe. Set enabled and visibility etc
- (void) prepareViewForExistingOwe
{
    [[self textFieldOweUsername] setUserInteractionEnabled:NO];
    [[self textField_oweAmountTotal] setUserInteractionEnabled:NO];
    [[self textField_oweDescription] setUserInteractionEnabled:NO];
    [[self tableView_payments] setAlpha:1.0f];
    [[self tableView_payments] setUserInteractionEnabled:YES];
    [[self btn_amountRemaining] setUserInteractionEnabled:YES];
}

// set the values of the UI Elements for the existing owe
- (void) setUIValuesForExistingOwe
{
    //set the title in nav bar
    NSString * title = [[self viewingOwe] oweToUsername];
    
    [self setTitle:title];
    
    //set the date created string
    NSString * dateStr = [NSDateFormatter localizedStringFromDate:[[self viewingOwe] dateCreated]
                                                        dateStyle:NSDateFormatterShortStyle
                                                        timeStyle:NSDateFormatterNoStyle];
    [[self label_dateCreated] setText:dateStr];
    
    //set the owe username
    [[self textFieldOweUsername] setText:[[self viewingOwe] oweToUsername]];
    
    //set the owe description
    [[self textField_oweDescription] setText:[[self viewingOwe] description]];
    
    // set the amount total
    float amountTotal = [[self viewingOwe] amountTotal];
    NSString* amountTotalStr = [NSString stringWithFormat:@"%.02f", amountTotal];
    [[self textField_oweAmountTotal] setText:amountTotalStr];
    
    // set the amount remaining
    float amountRemaining = [[self viewingOwe] amountRemaining];
    NSString* remainingValueStr = [NSString stringWithFormat:@"%.02f", amountRemaining];
    NSString * amountRemainingStr = [[self currencyString] stringByAppendingString:remainingValueStr];
    
    [[self btn_amountRemaining] setTitle:amountRemainingStr forState:UIControlStateNormal];
}

// Refresh the UI
- (void) refreshModelDependentUI
{
    [self setUIValuesForExistingOwe];
}

@end
