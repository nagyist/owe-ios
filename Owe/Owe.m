//
//  Owe.m
//  Owe
//
//  Created by Patrick Farrell on 19/10/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "Owe.h"
#import "OweDBDAO.h"
#import "NSString+Additions.h"

@implementation Owe
@synthesize oweID, oweFromUsername,
oweToUsername, description,
dateCreated, amountRemaining,
amountTotal, owePayments;

//- (void) createOweUniqueID
//{
//    [self setOweID:[NSString createGUIDString]];
//}

- (void) populateOwePayments
{
    OweDBDAO * dao = [OweDBDAO sharedInstance];
    [dao openDB];
    [self setOwePayments:[dao readAllOwePaymentsByOweID:[self oweID]]];
    [dao closeDB];
}


@end
