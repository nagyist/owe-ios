//
//  CustomColorsLoader.m
//  Owe
//
//  Created by Patrick Farrell on 30/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import "CustomColorsLoader.h"

@implementation CustomColorsLoader


// Return a UIColor pointer using the name from the Color in the CustomColors Plist
- (UIColor *) customColorWithName:(NSString *) customColorName withAlpha:(float) alphaVal
{
    //use the name of the color in param to get the hex value associated in the plist
    NSString * hexValue = [[self pListDictionary] objectForKey:customColorName];
    
    //use the UIColors category method to construct a UIColor from hex value
    UIColor * customColor = [UIColor colorFromHexString:hexValue alpha:alphaVal];
    
    return customColor;
}


@end
