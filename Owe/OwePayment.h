//
//  OwePayment.h
//  Owe
//
//  Created by Patrick Farrell on 10/11/2013.
//  Copyright (c) 2013 PatrickFarrellApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OwePayment : NSObject

// **** Members **** //

@property (nonatomic, strong) NSString * owePaymentID;
@property (nonatomic, strong) NSString * oweID;
@property (nonatomic, strong) NSDate * datePaid;
@property (nonatomic) float amountPaid;


@end
